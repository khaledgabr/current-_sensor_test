
#define RPWM 5 // define pin 3 for RPWM pin (output)
#define R_EN 4 // define pin 2 for R_EN pin (input)
#define R_IS A0 // define pin 5 for R_IS pin (output)

#define LPWM 6// define pin 6 for LPWM pin (output)
#define L_EN 7 // define pin 7 for L_EN pin (input)
#define L_IS A1 // define pin 8 for L_IS pin (output)
#define CW 1 //do not change
#define CCW 0 //do not change
#define debug 0 //change to 0 to hide serial monitor debugging infornmation or set to 1 to view

#include <RobojaxBTS7960.h>

RobojaxBTS7960 motor(R_EN, RPWM, R_IS, L_EN, LPWM, L_IS, debug);









#define VIN A0 // define the Arduino pin A0 as voltage input (V in)
const float VCC   = 5.0;// supply voltage is from 4.5 to 5.5V. Normally 5V.
const int model = 0;   // enter the model number (see below)

float cutOffLimit = 0.5;// set the current which below that value, doesn't matter. Or set 0.5

/*
          "ACS712ELCTR-05B-T",// for model use 0
          "ACS712ELCTR-20A-T",// for model use 1
          "ACS712ELCTR-30A-T"// for model use 2
  sensitivity array is holding the sensitivy of the  ACS712
  current sensors. Do not change. All values are from page 5  of data sheet
*/
float sensitivity[] = {
  0.185,// for ACS712ELCTR-05B-T
  0.100,// for ACS712ELCTR-20A-T
  0.066// for ACS712ELCTR-30A-T

};


const float QOV =   0.5 * VCC ;// set quiescent Output voltage of 0.5V
float voltage;// internal variable for voltage

float voltage_raw;




void setup()
{
  pinMode (11, OUTPUT);
  Serial.begin(9600);
  motor.begin();
  //pinMode (A0,INPUT_PULLUP);
  pinMode (A0, INPUT);
  //---------------------------------------------- Set PWM frequency for D5 & D6 -------------------------------

  //TCCR0B = TCCR0B & B11111000 | B00000001;    // set timer 0 divisor to     1 for PWM frequency of 62500.00 Hz
  TCCR0B = TCCR0B & B11111000 | B00000010;    // set timer 0 divisor to     8 for PWM frequency of  7812.50 Hz
  //TCCR0B = TCCR0B & B11111000 | B00000011;    // set timer 0 divisor to    64 for PWM frequency of   976.56 Hz (The DEFAULT)
  //TCCR0B = TCCR0B & B11111000 | B00000100;    // set timer 0 divisor to   256 for PWM frequency of   244.14 Hz
  //TCCR0B = TCCR0B & B11111000 | B00000101;    // set timer 0 divisor to  1024 for PWM frequency of    61.04 Hz
  motor.rotate(100, CCW);
  delay(5000);
}

void loop()
{
  motor.rotate(100, CCW);

  if ( analogRead(VIN) > 515)
  {
    voltage_raw =  analogRead(VIN) - 515;
  }
  else
  {
    voltage_raw = 515 - analogRead(VIN) ;
  }

  //Robojax.com ACS712 Current Sensor
  voltage_raw =   (5.0 / 1023.0) * voltage_raw; // Read the voltage from sensor

  voltage =  voltage_raw  + 0.065 ;// 0.000 is a value to make voltage zero when there is no current
  float current = voltage / sensitivity[model];

  if (abs(current) > cutOffLimit ) {
    Serial.print("V: ");
    Serial.print(voltage); // print voltage with 3 decimal places
    Serial.print("V, I: ");
    Serial.print(current, 2); // print the current with 2 decimal places
    Serial.println("A");
  } else {
    Serial.println(voltage_raw);
  }
  delay(100);

  if (current > 2)
  {
    motor.rotate(0, CCW);
    alarm ();
    delay (5000);
  }

  //float current2 = analogRead (A0) * 0.060869;
  //Serial.println (current2, 3.f);
}

void alarm ()
{
  for (int i = 0 ; i < 8 ; i++)
  {
    digitalWrite(11, HIGH);
    delay(500);
    digitalWrite(11, LOW);
    delay(500);
    digitalWrite(11, HIGH);
    delay(500);
    digitalWrite(11, LOW);
    delay(500);
  }
  digitalWrite(11, HIGH);
  delay(3000);
  digitalWrite(11, LOW);

}
